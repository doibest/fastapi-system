"""user

Revision ID: 7c69cf6ff6fc
Revises: 
Create Date: 2024-03-15 17:55:21.348401

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '7c69cf6ff6fc'
down_revision: Union[str, None] = None
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('system_users',
    sa.Column('user_id', sa.VARCHAR(length=32), nullable=True, comment='用户id'),
    sa.Column('nickname', sa.VARCHAR(length=128), nullable=True, comment='用户昵称(显示用可更改)'),
    sa.Column('username', sa.VARCHAR(length=128), nullable=True, comment='用户名(不可更改)'),
    sa.Column('avatar', sa.VARCHAR(length=256), nullable=True, comment='用户头像'),
    sa.Column('hashed_password', sa.VARCHAR(length=128), nullable=False, comment='密码'),
    sa.Column('phone', sa.VARCHAR(length=16), nullable=True, comment='手机号'),
    sa.Column('gender', sa.SmallInteger(), server_default='0', nullable=True, comment='性别 0=未知 1=男 2=女'),
    sa.Column('register_time', sa.DateTime(), nullable=True, comment='注册事件'),
    sa.Column('last_login_time', sa.DateTime(), nullable=True, comment='上次登录时间'),
    sa.Column('last_login_ip', sa.VARCHAR(length=64), nullable=True, comment='上次登录IP'),
    sa.Column('register_ip', sa.VARCHAR(length=64), nullable=True, comment='注册IP'),
    sa.Column('weixin_openid', sa.VARCHAR(length=64), nullable=True, comment='微信openId'),
    sa.Column('country', sa.VARCHAR(length=64), nullable=True, comment='国家'),
    sa.Column('province', sa.VARCHAR(length=64), nullable=True, comment='省'),
    sa.Column('city', sa.VARCHAR(length=64), nullable=True, comment='市'),
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('create_time', sa.DateTime(), server_default=sa.text('now()'), nullable=True, comment='创建时间'),
    sa.Column('update_time', sa.DateTime(), server_default=sa.text('now()'), nullable=True, comment='更新时间'),
    sa.Column('is_delete', sa.Integer(), server_default='0', nullable=True, comment='逻辑删除:0=未删除,1=删除'),
    sa.PrimaryKeyConstraint('id'),
    comment='用户表'
    )
    op.create_index(op.f('ix_system_users_id'), 'system_users', ['id'], unique=False)
    op.create_index(op.f('ix_system_users_phone'), 'system_users', ['phone'], unique=True)
    op.create_index(op.f('ix_system_users_user_id'), 'system_users', ['user_id'], unique=True)
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_system_users_user_id'), table_name='system_users')
    op.drop_index(op.f('ix_system_users_phone'), table_name='system_users')
    op.drop_index(op.f('ix_system_users_id'), table_name='system_users')
    op.drop_table('system_users')
    # ### end Alembic commands ###
