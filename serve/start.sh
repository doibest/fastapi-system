#! /bin/bash



cd /var/www/serve/app
pkill -f gunicorn -9
nohup gunicorn main:app -c gunicorn.py >/dev/null 2>log &


echo $(date +%Y-%m-%d\ %H:%M:%S)

/bin/bash
