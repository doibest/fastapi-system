import os
from typing import List

from pydantic import AnyHttpUrl

DEBUG: bool = True
#
API_V1_STR: str = "/v1"
# SECRET_KEY
SECRET_KEY: str = "Wchime"

# jwt加密算法
JWT_ALGORITHM: str = "HS256"
# jwt token过期时间 60 minutes * 24 hours * 8 days = 8 days
ACCESS_ACCESS_TOKEN_EXPIRE_MINUTES: int = 60 * 60 * 24
# jwt token过期时间 60 minutes * 24 hours * 8 days = 8 days
ACCESS_REFRESH_TOKEN_EXPIRE_MINUTES: int = 60 * 60 * 24 * 8

# 根路径
# BASE_DIR: str = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
BASE_DIR: str = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# 项目信息
PROJECT_NAME: str = "API"
DESCRIPTION: str = ""
SERVER_NAME: str = "API_V1"
SERVER_HOST: str = "http://127.0.0.1:8020"
# BACKEND_CORS_ORIGINS is a JSON-formatted list of origins
# e.g: '["http://localhost", "http://localhost:4200", "http://localhost:3000", \
# "http://localhost:8080", "http://local.dockertoolbox.tiangolo.com"]'
# 跨域
BACKEND_CORS_ORIGINS: List[str] = ['*']
# mysql 配置
MYSQL_USERNAME: str = "root"
MYSQL_PASSWORD: str = "mysql123456"
MYSQL_HOST: str = "192.168.1.101"
MYSQL_DATABASE: str = 'tte'

SQLALCHEMY_DATABASE_URL = f"mysql+pymysql://{MYSQL_USERNAME}:{MYSQL_PASSWORD}@" \
                          f"{MYSQL_HOST}/{MYSQL_DATABASE}?charset=utf8mb4"

# redis配置
REDIS_HOST: str = "192.168.1.101"
REDIS_PASSWORD: str = "redis123456"
REDIS_DB: int = 1
REDIS_PORT: int = 6379
REDIS_URL: str = f"redis://:{REDIS_PASSWORD}@{REDIS_HOST}:{REDIS_PORT}/{REDIS_DB}?encoding=utf-8"


# MINIO_HOST = "192.168.1.123"
MINIO_HOST = "192.168.1.101"
MINIO_PORT = "9000"
MINIO_USER = 'minioadmin'
MINIO_PWD = 'minioadmin'
MINIO_BUCKET = 'media'
MINIO_PROXY = "http://127.0.0.1:9000"


GENERATE_TOPIC_LIST = ['generate_result']
GENERATE_REQUEST = 'generate_request'
GENERATE_GROUP = 'generate_queue_group'

