from app.db.base_class import Base, gen_uuid
from sqlalchemy import Boolean, Column, Integer, String, VARCHAR, BIGINT, SmallInteger, DateTime, ForeignKey
from datetime import datetime
from sqlalchemy_serializer import SerializerMixin
from sqlalchemy.orm import relationship


# SerializerMixin 有反向查询的表在使用to_dict()时一定要指定序列化字段，不然进入无限迭代


class Users(Base, SerializerMixin):
    """
    用户表
    """
    __tablename__ = "system_users"
    user_uuid = Column(VARCHAR(32), default=gen_uuid, index=True, unique=True, comment="用户id")
    nickname = Column(VARCHAR(128), comment="用户昵称")
    username = Column(VARCHAR(128), comment="用户名", unique=True, index=True)
    avatar = Column(VARCHAR(256), nullable=True, comment="用户头像")
    hashed_password = Column(VARCHAR(128), nullable=False, comment="密码")
    phone = Column(VARCHAR(16), unique=True, index=True, nullable=True, comment="手机号")
    gender = Column(SmallInteger, default=0, comment="性别 0=未知 1=男 2=女", server_default="0")
    register_time = Column(DateTime, default=datetime.now, comment="注册时间")
    last_login_time = Column(DateTime, default=datetime.now, comment="上次登录时间")
    country = Column(VARCHAR(64), nullable=True, comment="国家")
    province = Column(VARCHAR(64), nullable=True, comment="省")
    city = Column(VARCHAR(64), nullable=True, comment="市")
    is_active = Column(SmallInteger, default=1, comment="可用 0=否 1=是", server_default="1")
    is_superuser = Column(SmallInteger, default=0, comment="超级管理员 0=否 1=是", server_default="0")
    __table_args__ = ({'comment': '用户表'})


class Role(Base, SerializerMixin):
    """
    角色表
    """
    __tablename__ = "system_role"
    __table_args__ = ({'comment': '角色表'})
    name = Column(VARCHAR(128), comment="名字")
    sort = Column(Integer, default=1, comment="排序", server_default="1")
    status = Column(Boolean, default=True, comment="角色状态")
    admin = Column(Boolean, default=False, comment="是否为admin")
    remark = Column(VARCHAR(1024), nullable=True, comment="备注")


class Permission(Base, SerializerMixin):
    __tablename__ = "system_permission"
    __table_args__ = ({'comment': '权限表'})
    name = Column(VARCHAR(256), comment="名字")
    url = Column(VARCHAR(512), comment="接口地址")
    group = Column(VARCHAR(256), nullable=True, comment="分组")
    method = Column(VARCHAR(16), comment="请求方法")


class UserRole(Base, SerializerMixin):
    __tablename__ = "system_user_role"
    __table_args__ = ({'comment': '用户角色表'})
    user_id = Column(Integer, ForeignKey('system_users.id', ondelete='CASCADE'))
    user = relationship('Users', backref='role')    # , lazy='dynamic'
    role_id = Column(Integer, ForeignKey('system_role.id', ondelete='CASCADE'))
    role = relationship('Role', backref='user')


class RolePermission(Base, SerializerMixin):
    __tablename__ = "system_role_permission"
    __table_args__ = ({'comment': '角色权限表'})
    permission_id = Column(Integer, ForeignKey('system_permission.id', ondelete='CASCADE'))
    permission = relationship('Permission', backref='role')
    role_id = Column(Integer, ForeignKey('system_role.id', ondelete='CASCADE'))
    role = relationship('Role', backref='permission')




