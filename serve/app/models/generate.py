from app.db.base_class import Base, gen_uuid
from sqlalchemy import Boolean, Column, Integer, String, VARCHAR, BIGINT, SmallInteger, DateTime, ForeignKey, Text
from datetime import datetime
from sqlalchemy_serializer import SerializerMixin
from sqlalchemy.orm import relationship
from app.models.system import Users


class GenerateRecord(Base, SerializerMixin):
    __tablename__ = "generate_record"
    __table_args__ = ({'comment': '生成记录表'})

    user_id = Column(Integer, ForeignKey('system_users.id', ondelete='CASCADE'))

    params = Column(Text, nullable=True, comment="参数")
    number = Column(Integer, default=1, comment="数量", server_default="1")
    type = Column(Integer, default=1, comment="类型", server_default="1")
    state = Column(Integer, default=1, comment="状态", server_default="1")


class GenerateParamsImages(Base, SerializerMixin):
    __tablename__ = "generate_params_images"
    __table_args__ = ({'comment': '生成参数图片表'})

    record_id = Column(Integer, ForeignKey('generate_record.id', ondelete='CASCADE'))
    record = relationship('GenerateRecord', backref='paramsimages')

    type = Column(VARCHAR(128), comment="类型", nullable=True)
    path = Column(VARCHAR(512), comment="路径")


class GenerateResult(Base, SerializerMixin):
    __tablename__ = "generate_result"
    __table_args__ = ({'comment': '生成结果表'})

    record_id = Column(Integer, ForeignKey('generate_record.id', ondelete='CASCADE'))
    record = relationship('GenerateRecord', backref='result')

    seed = Column(VARCHAR(128), comment="随机种子", default="-1")
    path = Column(VARCHAR(512), comment="路径")






