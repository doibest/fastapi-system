from fastapi import APIRouter
from .view.users_view import router as user_routor
from .view.role_view import router as role_routor
from .view.permission_view import router as permission_routor
router = APIRouter()

router.include_router(user_routor, prefix="", tags=["用户"])
router.include_router(role_routor, prefix="", tags=["角色"])
router.include_router(permission_routor, prefix="", tags=["权限"])




