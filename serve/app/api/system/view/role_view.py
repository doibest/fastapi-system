from typing import Any

from fastapi import Depends
from sqlalchemy.orm import Session

from app.api.system.schemas import role_schema
from app.api.system.crud.role_crud import crud_role, crud_role_permission
from app.db.database import get_db
from app.models.system import Users
from app.utils import security
from app.utils.custom_response import success_response
from fastapi import APIRouter, Query, Body, Path

router = APIRouter()

# 使用relationship和to_dict时一定要指定only字段


@router.post("/create_role", summary="新增角色", response_model=role_schema.CreateRole, name='新增角色')
async def create_role(
    *,
    db: Session = Depends(get_db),
    role_info: role_schema.CreateRole,
    current_user: Users = Depends(security.verify_admin_permission),
) -> Any:

    role = crud_role.create(db, data=role_info)

    return success_response(data=role.to_dict(only=('id', 'name', 'sort', 'status', 'admin', 'remark')))


@router.post("/delete_role", summary="删除角色", response_model=role_schema.CreateRole, name='删除角色')
async def delete_role(
    *,
    db: Session = Depends(get_db),
    req_data: role_schema.DeleteRole,
    current_user: Users = Depends(security.verify_admin_permission),
) -> Any:

    crud_role.delete(db, req_body=req_data)

    return success_response(data={})


@router.post("/update_role/{role_id}", summary="更新角色", response_model=role_schema.CreateRole, name='更新角色')
async def update_role(
    *,
    db: Session = Depends(get_db),
    role_id,
    role_info: role_schema.UpdateRole,
    current_user: Users = Depends(security.verify_admin_permission),
) -> Any:

    instance = crud_role.get_object(db, role_id)
    instance = crud_role.update(db, instance=instance, data=role_info)

    return success_response(data=instance.to_dict(only=('id', 'name', 'sort', 'status', 'admin', 'remark')))


@router.get("/get_role_list", summary="获取角色列表", response_model=role_schema.Role, name='获取角色列表')
async def get_role_list(
    *,
    db: Session = Depends(get_db),
    page: int = Query(1, description='页码', ge=1),
    page_size: int = Query(10, description='页面大小', le=50, ge=1),
    search_info: str = Query(None, description='搜索内容'),
    current_user: Users = Depends(security.verify_admin_permission),
) -> Any:
    queryset, total = crud_role.get_page_queryset(db, page=page, page_size=page_size, search_info=search_info)
    items = crud_role.serializer(queryset, only=('id', 'name', 'sort', 'status', 'admin', 'remark'))
    resp = {
        "items": items,
        'total': total
    }
    return success_response(data=resp)


@router.post("/add_role_permission", summary="新增角色权限", response_model=role_schema.SetRolePermission, name='新增角色权限')
async def add_role_permission(
    *,
    db: Session = Depends(get_db),
    req_body: role_schema.SetRolePermission,
    current_user: Users = Depends(security.verify_admin_permission),
):
    crud_role_permission.batch_create(db, data=req_body)
    resp = {}
    return success_response(data=resp)


@router.post("/modify_role_permission", summary="修改角色权限", response_model=role_schema.SetRolePermission, name='修改角色权限')
async def add_role_permission(
    *,
    db: Session = Depends(get_db),
    req_body: role_schema.SetRolePermission,
    current_user: Users = Depends(security.verify_admin_permission),
):
    crud_role_permission.modify(db, data=req_body)
    resp = {}
    return success_response(data=resp)





