from typing import Optional, Union

from pydantic import BaseModel, Field


class CreatePermission(BaseModel):
    name: str = Field(description='名字')
    url: str = Field(description='接口地址')
    group: Optional[str] = Field(description='分组')
    method: str = Field(description='请求方法')


class Permission(BaseModel):
    name: str = Field(description='名字')
    url: str = Field(description='接口地址')
    group: Optional[str] = Field(description='分组')
    method: str = Field(description='请求方法')


class UpdatePermission(BaseModel):
    name: Optional[str] = Field(description='名字')
    url: Optional[str] = Field(description='接口地址')
    group: Optional[str] = Field(description='分组')
    method: Optional[str] = Field(description='请求方法')


class DeletePermission(BaseModel):
    id: list[int] = Field(description='ID列表')

