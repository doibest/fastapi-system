#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/7/7 16:23
# @Author  : CoderCharm
# @File    : user_schema.py
# @Software: PyCharm
# @Desc    :
"""

"""

from typing import Optional, Union

from pydantic import BaseModel, EmailStr, AnyHttpUrl, Field


# 登录认证 验证数据字段都叫username
class UsernameAuth(BaseModel):
    password: str
    username: str
    grant_type: Optional[str]


# -------------------------  登录返回  --------------------------------------
class LoginToken(BaseModel):
    access_token: str = Field(description='登录token')
    refresh_token: str = Field(description='刷新token')


class LoginResp(BaseModel):
    id: int = Field(description='ID')
    nickname: str = Field(description='用户昵称')
    username: str = Field(description='用户名')
    avatar: str = Field(description='头像')
    token: LoginToken


# 注册
class UserRegister(BaseModel):
    nickname: str = Field(description='用户昵称')
    username: str = Field(description='用户名')
    password: str = Field(description='密码')
    password1: str = Field(description='确认密码')
    phone: Optional[str] = Field(description='手机号')
    gender: Optional[int] = Field(description='性别 0=未知 1=男 2=女', default=0)
    country: Optional[str] = Field(description='国家')
    province: Optional[str] = Field(description='省')
    city: Optional[str] = Field(description='市')


# 创建账号需要验证的条件
class UserCreate(UserRegister):

    is_active: Optional[str] = Field(description='可用 0=否 1=是', default=1)


class RespUserInformation(BaseModel):
    id: int = Field(description='ID')
    nickname: str = Field(description='用户昵称')
    username: str = Field(description='用户名')
    avatar: Union[str, None] = Field(description='头像')
    user_uuid: Union[str, None] = Field(description='用户id')
    phone: Union[str, None] = Field(description='手机号')
    gender: int = Field(description='性别 0=未知 1=男 2=女')
    register_time: str = Field(description='注册时间')
    country: Union[str, None] = Field(description='国家')
    province: Union[str, None] = Field(description='省')
    city: Union[str, None] = Field(description='市')
    is_superuser: int = Field(description='超级管理员 0=否 1=是')


class ModifyUserInfor(BaseModel):

    nickname: Optional[str] = Field(description='用户昵称')
    username: Optional[str] = Field(description='用户名')
    user_uuid: Optional[str] = Field(description='用户id')
    phone: Optional[str] = Field(description='手机号')
    gender: Optional[int] = Field(description='性别 0=未知 1=男 2=女')
    country: Optional[str] = Field(description='国家')
    province: Optional[str] = Field(description='省')
    city: Optional[str] = Field(description='市')


# 刷新token
class RefreshToken(BaseModel):
    refresh_token: str


# 修改密码
class UpdatePassword(BaseModel):
    password0: str = Field(description='原密码')
    password1: str = Field(description='新密码')
    password2: str = Field(description='确认密码')


# 返回的用户信息
class UserInfo(BaseModel):
    role_id: int
    role: str
    nickname: str
    avatar: AnyHttpUrl


class UserUpdate(ModifyUserInfor):
    pass


class SetUserRole(BaseModel):
    user_id: int = Field(description='用户ID')
    role: list[int] = Field(description='角色ID列表')


# class UserBase(BaseModel):
#     email: Optional[EmailStr] = None
#     phone: int = None
#     is_active: Optional[bool] = True
#
#
# class UserAuth(BaseModel):
#     password: str
#
#
# # 手机号登录认证 验证数据字段都叫username
# class UserPhoneAuth(UserAuth):
#     username: int
#
#
#
#
#
# # Properties to receive via API on update
# class UserUpdate(UserBase):
#     password: Optional[str] = None
#
#
# class UserInDBBase(UserBase):
#     id: Optional[int] = None
#
#     class Config:
#         orm_mode = True
#
#
# class UserInDB(UserInDBBase):
#     hashed_password: str
#
#
#
#
#
# class RespBase(BaseModel):
#     code: int
#     message: str
#     data: Union[dict, list, str]
#
#
# class RespUserInfo(RespBase):
#     # 响应用户信息
#     data: UserInfo
#
#
# class Token(BaseModel):
#     token: str
#
#
# class TokenPayload(BaseModel):
#     sub: Optional[int] = None
#
#
# class RespToken(RespBase):
#     # 认证响应模型
#     data: Token
