from typing import Optional, Union

from pydantic import BaseModel, Field


class CreateRole(BaseModel):
    name: str = Field(description='名字')
    sort: Optional[int] = Field(description='排序', default=1)
    status: Optional[bool] = Field(description='状态', default=True)
    admin: Optional[bool] = Field(description='是否为admin', default=False)
    remark: Optional[str] = Field(description='备注')


class Role(BaseModel):
    name: str = Field(description='名字')
    sort: int = Field(description='排序', default=1)
    status: bool = Field(description='状态', default=True)
    admin: bool = Field(description='是否为admin', default=False)
    remark: str = Field(description='备注')


class UpdateRole(BaseModel):
    name: Optional[str] = Field(description='名字')
    sort: Optional[int] = Field(description='排序', default=1)
    status: Optional[bool] = Field(description='状态', default=True)
    admin: Optional[bool] = Field(description='是否为admin', default=False)
    remark: Optional[str] = Field(description='备注')


class DeleteRole(BaseModel):
    id: list[int] = Field(description='ID列表')


class SetRolePermission(BaseModel):
    role: int = Field(description='角色ID')
    permission: list[int] = Field(description='权限ID列表')

