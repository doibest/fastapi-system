from typing import Optional, Union

from pydantic import BaseModel, Field


class Generate1(BaseModel):
    params: Optional[str] = Field(description='参数', default="{}")
    prompt_text: Optional[str] = Field(description='手写参数')
    cfg_scale: int = Field(description='相关性', default=7)
    seed: int = Field(description='随机种子', default=-1)
    width: int = Field(description='宽', default=512)
    height: int = Field(description='高', default=512)
    batch_size: int = Field(description='数量', default=1)
    sub_clothingType: Optional[str] = Field(description='子款式', default="新中式连衣裙")
    control_weight: int = Field(description='数量', default=1)
    fabric_congtrol_weight: int = Field(description='数量', default=1)


