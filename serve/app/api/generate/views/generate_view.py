import json
import traceback
from typing import Any

from fastapi import Depends
from sqlalchemy.orm import Session

from app.api.generate.schemas import generate_schema
from app.db.database import get_db
from app.utils import security
from app.utils.file_storage import save_storage_file, get_storage_file
from app.utils.logger import logger
from app.utils.custom_response import success_response, error_response

from fastapi import APIRouter, File, UploadFile, Query
from fastapi import HTTPException, status, Request

router = APIRouter()


@router.post("/start_generate1", summary="", response_model=None, name='')
async def modify_user_role(
        request: Request,
        *,
        db: Session = Depends(get_db),
        req_body: generate_schema.Generate1,
        fabric_img: UploadFile = File(),
        reference_type_img: UploadFile = File(),

) -> Any:

    return success_response(data={})

















