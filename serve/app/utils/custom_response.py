from typing import Union
from fastapi.responses import JSONResponse, Response


def success_response(data: Union[list, dict, str] = None, message: str = "success"):
    return JSONResponse(
        content={
            'code': 1,
            'message': message,
            'data': data,
        }
    )


def error_response(data: Union[list, dict, str] = None, message: str = "error", code: int = 400) -> Response:
    return JSONResponse(
        content={
            'code': code,
            'message': message,
            'data': data,
        }
    )

