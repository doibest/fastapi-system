import datetime
import hashlib
import random
import time


def get_now_time_string():
    """
        :return:
        :PS ：并发较高时尾部随机数增加
    """
    order_id = str(datetime.datetime.fromtimestamp(time.time())).replace("-", "").replace(" ", "").replace(":","").replace(".", "") + str(random.randint(100, 99999))
    return order_id


def getSha256(text):
    h = hashlib.sha256()
    h.update(text.encode())
    return h.hexdigest()


def get_router_list(app):
    exclude_li = ['/assets', '/docs/oauth2-redirect', '/v1/docs', '/v1/openapi.json', '/v1/redoc']
    for route in app.routes:
        if route.path not in exclude_li:
            print(route.path, route.name, route.tags, route)


def get_request_ip(request):
    """
    获取请求IP
    :param request:
    :return:
    """
    # print(request.headers)
    x_forwarded_for = request.headers.get('x-forwarded-for')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
        return ip
    ip = request.headers.get('remote_addr')
    return ip or 'unknown'

