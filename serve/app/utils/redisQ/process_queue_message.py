import asyncio
import json
import threading
import traceback

from app.utils.websocket_utils import publish_message
from app.views.websocket_view import websocket_manager
# # --------------------------------------  处理消息  -----------------------------------------------------


def process_messages(stream, message_data, message_id):
    try:
        message_data = json.loads(message_data['data'])
        generate_type = message_data.get('generate_type')
        record_id = message_data.get('record_id')
        tips_message = message_data.get('tips_message', None)
        print(message_data)
        # if generate_type == "matting":
        #     user_id = record_id
        #     user = ld_models.Loginuser.objects.filter(id=user_id).last()
        #     if user:
        #         print("matting", user_id)
        #         ret_data = {'img_list': message_data.get('img_base64_list'), 'state': 'success' if message_data.get('img_base64_list') else 'fail', 'record_id': record_id, 'user_id': user_id,
        #                     'generate_type': generate_type, 'tips_msg': tips_message}
        #         send_message(group_name=user.uid, message_type='generate_result', message=ret_data)
        # else:
        #     process_generate_result(message_data.get('img_base64_list'), message_data.get('seed_list'), record_id, tips_message)


        asyncio.run(publish_message("aaa", message_data, "generate"))
        # threading.Thread(target=websocket_manager.send_other_message, args=("aaa", json.dumps({"name": "aaa", "age": 18}))).start()

    except Exception as e:
        print('msg process fail!')
        print(traceback.format_exc())
