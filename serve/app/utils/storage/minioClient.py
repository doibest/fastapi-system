import copy
import os
import traceback

from minio import Minio
from app.conf import settings

MINIO_CONF = {
    'endpoint': settings.MINIO_HOST + ':' + settings.MINIO_PORT,
    'access_key': settings.MINIO_USER,
    'secret_key': settings.MINIO_PWD,
    'secure': False
}

client = Minio(**MINIO_CONF)


def get_file_size(file):
    """
    获取文件大小
    file: bytes
    return: int
    """
    file.seek(0, os.SEEK_END)
    return file.tell()
    # im = io.BytesIO(file)
    # return im.getbuffer().nbytes


def go_upload_file(file, bucket='media', path_name=''):
    """
    上传文件
    """
    try:
        # print(path_name)
        # print(type(file))
        file_len = get_file_size(copy.copy(file))
        # print(file_len)
        client.put_object(bucket_name=bucket, object_name=path_name, data=file, length=file_len, part_size=0)
        return 1
    except Exception as e:
        print(e)
        return 0


def go_upload_file_have_size(file, size=-1, part_size=0, bucket='media', path_name='', **kwargs):
    """
    上传文件，已有文件大小
    """
    try:
        client.put_object(bucket_name=bucket, object_name=path_name, data=file, length=size, part_size=part_size, **kwargs)
        return 1
    except Exception as e:
        # print(file, size, path_name)
        print(traceback.format_exc())
        return 0


def go_delete_file(bucket='media', path_name=''):
    """
    删除文件
    """
    try:
        # print(bucket, path_name)
        client.remove_object(bucket_name=bucket, object_name=path_name)
        return 1
    except Exception as e:
        print(e)
        return 0


def go_delete_file_list(bucket='media', path_name_list=[]):
    """
    删除文件列表
    未实现，据说需要删除完遍历结果
    """
    try:
        ret = client.remove_objects(bucket, path_name_list, bypass_governance_mode=False)
        print(ret)
        return 1
    except Exception as e:
        print(e)
        return 0


def get_file_url(bucket='media', path_name=''):
    """
    获取文件url
    """
    try:
        url = client.presigned_get_object(bucket_name=bucket, object_name=path_name)
        return url
    except Exception as e:
        print(e)
        return None


def get_file_path(path):
    path = path.split('/')[2:]
    final_path = '/'.join(path)
    return final_path

