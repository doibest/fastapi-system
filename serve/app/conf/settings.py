import os
from typing import List

import json
from configparser import ConfigParser
from app.utils.encryption import getDataAes
conf = ConfigParser()
conf.read("config.ini")


try:
    SECRET_KEY = conf.get('serve', 'secret')
    DEBUG = conf.getboolean('serve', 'debug')
    ALLOWED_HOSTS = conf.get('serve', 'allow_host')

    MYSQL_HOST = conf.get("mysql", "host")
    MYSQL_PORT = conf.getint("mysql", "port")
    MYSQL_USERNAME = conf.get("mysql", "user")
    MYSQL_PASSWORD = conf.get("mysql", "password")
    MYSQL_DATABASE = conf.get("mysql", "database")

    REDIS_HOST = conf.get("redis", "host")
    REDIS_PORT = conf.getint("redis", "port")
    REDIS_PASSWORD = conf.get("redis", "password")
    REDIS_DB = conf.getint("redis", "db")

    MINIO_HOST = conf.get("minio", "client_host")
    MINIO_PORT = conf.get("minio", "client_port")
    MINIO_PWD = conf.get("minio", "password")
    MINIO_USER = conf.get("minio", "user")
    MINIO_PROXY = conf.get("minio", "proxy")
    MINIO_BUCKET = conf.get('minio', 'bucket')


except Exception as e:

    raise Exception('请检查配置文件，格式不正确')


try:
    ALLOWED_HOSTS = json.loads(ALLOWED_HOSTS)
except:
    raise Exception('请检查配置文件，格式不正确')

MYSQL_USERNAME = getDataAes(SECRET_KEY, MYSQL_USERNAME)
MYSQL_PASSWORD = getDataAes(SECRET_KEY, MYSQL_PASSWORD)
REDIS_PASSWORD = getDataAes(SECRET_KEY, REDIS_PASSWORD)
MINIO_PWD = getDataAes(SECRET_KEY, MINIO_PWD)
MINIO_USER = getDataAes(SECRET_KEY, MINIO_USER)

if MYSQL_USERNAME and MYSQL_PASSWORD and REDIS_PASSWORD and MINIO_PWD and MINIO_USER:
    pass
else:
    raise Exception('请检查配置文件，解密失败')

# ----------------------------------------------------------------------------------------------
# 根路径
BASE_DIR: str = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

API_V1_STR: str = "/v1"
# jwt加密算法
JWT_ALGORITHM: str = "HS256"
# jwt token过期时间 60 minutes * 24 hours * 8 days = 8 days
ACCESS_ACCESS_TOKEN_EXPIRE_MINUTES: int = 60 * 60 * 24
# jwt token过期时间 60 minutes * 24 hours * 8 days = 8 days
ACCESS_REFRESH_TOKEN_EXPIRE_MINUTES: int = 60 * 60 * 24 * 8


# 项目信息
PROJECT_NAME: str = "API"
DESCRIPTION: str = ""
SERVER_NAME: str = "API_V1"
SERVER_HOST: str = "http://127.0.0.1:8000"


# 跨域
BACKEND_CORS_ORIGINS: List[str] = ALLOWED_HOSTS


SQLALCHEMY_DATABASE_URL = f"mysql+pymysql://{MYSQL_USERNAME}:{MYSQL_PASSWORD}@" \
                          f"{MYSQL_HOST}:{MYSQL_PORT}/{MYSQL_DATABASE}?charset=utf8mb4"

REDIS_URL: str = f"redis://:{REDIS_PASSWORD}@{REDIS_HOST}:{REDIS_PORT}/{REDIS_DB}?encoding=utf-8"


DEFAULT_ROLE: List[dict] = [
        {"name": "管理员", "sort": 1, "admin": True},
    ]

DEFAULT_USER: List[dict] = [
        {"nickname": "superadmin", "username": "superadmin", "password": "superadmin", "is_superuser": 1},
        {"nickname": "admin", "username": "admin", "password": "admin", "admin": 1},
    ]





