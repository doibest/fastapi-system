from fastapi import APIRouter

from app.api import system
from app.views.websocket_view import router as websocket_router

api_router = APIRouter()
api_router.include_router(websocket_router, prefix="", tags=["websocket"])
api_router.include_router(system.router, prefix="/user")







