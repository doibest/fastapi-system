import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from app.db.init_db import init_db_data
from app.db.session import SessionLocal

from app.api.system.crud.permission_crud import auto_create_router_permission


def init():
    print("Creating initial data")
    db = SessionLocal()
    init_db_data(db)

    from app.factory import create_app

    auto_create_router_permission(create_app())

    print("Initial data created")


if __name__ == '__main__':

    init()


